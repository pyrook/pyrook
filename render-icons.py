#! /usr/bin/env python3
import subprocess

from PyQt5.Qt import Qt
from PyQt5.QtGui import QIcon, QGuiApplication

app = QGuiApplication([])
m = QIcon("PyRook/data/pyrook_logo.svg")
fs = []
for size in (16,32,64,128,256):
    r = m.pixmap(size,size)
    fn = "PyRook/data/pyrook_logo_{}.png".format(size)
    fs.append(fn)
    r.save(fn)

subprocess.check_call(["convert"] + fs + ["PyRook/data/pyrook_logo.ico"])
