name = "PyRook"

short_description = "A standalone client for RookChat written in Python and Qt"

long_description = """PyRook aims to provide a platform-independent standalone [RookChat](http://rinkworks.com/rookchat) client with feature parity with the standard web interface, plus additional features such as highlighting on your username and local logging. By default it connects to [RinkChat](http://rinkworks.com/rinkchat), [RinkWorks](http://rinkworks.com)' installation of RookChat.

Comments, bugs, etc may be memoed to Sentynel or #pyrook on RinkChat, or emailed to me.

PyRook is available on [PyPI](https://pypi.python.org/pypi/PyRook). PyRook's source code is public domain. It is hosted on [Gitlab](https://gitlab.com/) and can be accessed [here](https://gitlab.com/pyrook/pyrook)."""

priority = 0

categories = "misc"

requirements = """Instructions for installing PyRook and its dependencies for specific operating systems are included below. PyRook is cross-platform, and the general requirements are Python 3.3 or greater, PyQt4 or PyQt5, and lxml. pyenchant or hunspell-cffi are optional and provide spellcheck support.

### Windows/OS X
The recommended installation method for Windows and OS X is [Anaconda](https://www.continuum.io/downloads). If you don't use Python otherwise, install [Miniconda](http://conda.pydata.org/miniconda.html)'s Python 3.x 64-bit version. If prompted, you want to make this your default version of Python and add it to your PATH.

On Windows, you can then use [this script](https://sentynel.com/media/downloads/install-pyrook.cmd) to install PyRook. You may get security warnings - I promise it's safe! If the script throws an error that 'conda' can't be found, you need to start it from the command prompt. Open your downloads folder, hold shift, right click, and choose "open command window here". Then type `install-pyrook`. Either way, say yes if prompted.

When the script is done, PyRook should open. It will be added to your start menu, and you can pin it to the taskbar from there if you wish. (Pinning the running app doesn't always work properly.)

Alternatively, to install manually, do the following: Open a command prompt (on Windows, open start menu -> type `cmd`; on OS X find Terminal in Applications) and run the following command to install PyRook's dependencies:

`conda install pip setuptools pyqt5 lxml cffi pywin32`

(Don't include pywin32 on OS X.) Say yes at any prompts. Once that has completed you can install PyRook itself and the spellchecker:

`pip install pyrook hunspell-cffi`

Note that hunspell-cffi is not currently available for every OS; if installing it fails PyRook itself will still work.

Finally, start PyRook:

`pyrook`

### Linux
All required packages should be available from your distribution's package manager. On *ubuntu, install `python3-pip python3-pyqt5 python3-pyqt5.qtwebkit python3-lxml python3-enchant`. All other dependencies will be pulled in automatically if necessary.

You can then simply `pip3 install pyrook` and run `pyrook` to start. A desktop file will be installed into your application menu.
"""

versions = [
{"name":"Initial Release", "major":0, "minor":1, "point":0, "description":"First Qt based release. Should implement all basic functions, and most functions from the old wxWidgets based version."},
{"major":0, "minor":1, "point":1, "description":"Lots of minor fixes: /purge, quit keyboard shortcut on Windows, open external links, sending messages with non-ASCII characters, more reliable chatstream merging, more reliable scrolling down on new message."},
{"major":0, "minor":1, "point":2, "description":"More fixes: smileys, extraneous comma in some room names, purging, XML syntax error handling. Crash prevention when displaying room list and room with bots and no topic visible (though user list still isn't displayed in this case). Possible fix for intermittent crash on user list load."},
{"major":0, "minor":1, "point":3, "description":"Features: highfive command, glow on input box when /msg targets selected. Fixes: URL handling (internal links, external links with special characters, links on PySide v1.0.3), userlist request crash (probably - please report if you still get it), parsing improvements, sane application exiting (individual rooms can be left, doesn't halt system shutdown, etc), room list window topic wrapping, misc cleanup."},
{"major":0, "minor":2, "point":0, "description":"Changes since v0.1.3 include a better room join dialogue which will display any combination of users, topic and bots and has new room buttons; window alerts (taskbar highlight etc) on new messages or messages mentioning particular words; a permanent fix for chatstream merging and timestamp related bugs; and lots of miscellaneous fixes and tweaks."},
{"major":0, "minor":2, "point":1, "description":"Configurable font size, improvements to highlighting handling, RookChat options, memos, help etc in menus, and fixes to room join dialogue for non-English languages."},
{"major":0, "minor":2, "point":2, "description":"Remember username/password, command line options for user/pass/server, embedded QtWebKit tabs for internal RookChat links, logo/window icon, an internal debug log window, and more tweaks and fixes."},
{"major":0, "minor":2, "point":3, "description":"Tweaks: skip the login box if a username and password are remembered, and allow remembering the size of the room join dialogue."},
{"major":0, "minor":2, "point":4, "description":"Tweaks: Fix external links from web tabs, and alter the logo for better display against dark backgrounds."},
{"major":0, "minor":2, "point":5, "description":"Really boring: Fix encoding-related issues, including broken room join dialogue with non-ASCII topics and send/receive inconsistencies for Windows-1252 encoded characters such as smart quotes."},
{"major":0, "minor":2, "point":6, "description":"Add automatic focus on the input box in a number of cases, many debug improvements including exceptions and a raw HTML view, a fix for a Windows issue with the chatstream dying, and some minor fixes."},
{"major":0, "minor":3, "point":0, "description":"New features since v0.2.6 include: optional spellcheck; greatly improved alert configurability and accuracy, including different settings when away; optional reminder of away status; and optional GeoIP lookups for op use. Fixes include: a Windows issue causing occasional line loss; correct result from right click -> Copy Link Location; some WebTab behaviour such as refresh and copy (on Windows); and other miscellaneous fixes."},
{"major":0, "minor":3, "point":1, "description":"Add the ability to /purge without losing the entire backlog. Bug fixes including size and behaviour of input box, away state tracking, the /msg target box behaviour if the targeted user leaves, and the selection clipboard on Linux."},
{"major":0, "minor":3, "point":2, "description":"Adds logging functionality and the option to display timestamps in the local timezone. Fixes the improved purge handling and numerous minor glitches, and tidies the settings dialogue a bit."},
{"major":0, "minor":3, "point":3, "description":"Adds option to split private messages into separate tabs. Minor speed improvement to chatstream parsing, and a couple of minor fixes."},
{"major":0, "minor":3, "point":4, "description":"Minor bugfixes. Removal of unused and broken tab detachment feature. Tools to distribute standalone Windows package."},
{"major":0, "minor":3, "point":5, "description":"Adds a find function, detection of URLs in the topic, and the ability to show large versions of images in bot games. Improves message tab handling of bots and status changes. Fixes numerous minor issues including parsing and logging failures and incorrect spellcheck tokeniser behaviour."},
{"major":0, "minor":3, "point":6, "description":"Minor fixes: Userlist parse failure in rare cases, CPU eating bug on dead network connection."},
{"major":0, "minor":3, "point":7, "description":"Adds support for proxies. Fixes errors with Qt 4.8."},
{"major":0, "minor":3, "point":8, "description":"Add notification on network connectivity issues and ability to scroll back through sent message history. Fix a chatstream refreshing issue after being kicked."},
{"major":0, "minor":3, "point":9, "description":"Fix showing timestamps in local time and some odd topic link behaviour. Improve detection of network errors."},
{"major":0, "minor":3, "point":10, "description":"Fix a crash displaying the userlist with non-ASCII labels."},
{"major":0, "minor":3, "point":11, "description":"Adds a choice of log rotation frequency and a timestamp toggle for users who normally have timestamps off. Improves appearance of web tabs. Fixes bugs with long format timestamps in local time and userlist IP lookup."},
{"major":0, "minor":3, "point":12, "description":"Adds shortcuts for timestamp toggling and focusing the input bar. Various fixes to parsing, timestamp toggling, and more."},
{"major":0, "minor":3, "point":13, "description":"Fix some images in the chat stream, segfaulting on loading QtWebKit with some versions of Qt, and some encoding issues on Python 3."},
{"major":0, "minor":4, "point":0, "description":"Add font choice configuration, improve detection of the stream dropping, and fixes including opening the log directory on Windows."},
{"major":0, "minor":4, "point":1, "description":"Better handling of images."},
{"major":0, "minor":4, "point":2, "description":"Ability to edit saved login details."},
{"major":0, "minor":4, "point":3, "description":"Fix security flaw allowing PyRook users to be kicked from the room with a crafted message."},
{"major":0, "minor":5, "point":0, "description":"Move to Python 3 only and PyQt, and move to distribution using PyPI. Add built-in updater. Add smiley hovertext."},
{"major":0, "minor":5, "point":1, "description":"Qt5 compatibility fix."},
{"major":0, "minor":5, "point":2, "description":"Add support for refreshing mode for compatibility with some proxies."},
{"major":0, "minor":5, "point":3, "description":"Fix logging Unicode issue on Windows and improve support for high DPI screens."},
]
