PyRook
======

This is a standalone cross-platform client for the
`RookChat <http://rinkworks.com/rookchat>`_ chat server, primarily
`RinkChat <http://rinkworks.com/rinkchat>`_. It provides feature parity with the
web interface and a number of additional features, including infinite
scrollback, logging, and separate tabs for private messages.
