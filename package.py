#! /usr/bin/env python3
import argparse
import glob
import hashlib
import os
import shutil
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("action", choices=["all", "build", "sign", "pypi"], default="all", nargs="*")
args = parser.parse_args()

def should(x):
    return "all" in args.action or x in args.action

if should("build"):
    # build python package
    if os.path.exists("dist/"):
        shutil.rmtree("dist/")
    subprocess.check_call(["python3", "setup.py", "sdist"])
    subprocess.check_call(["python3", "setup.py", "bdist_wheel"])

if should("sign"):
    # sign packages
    for i in os.listdir("dist/"):
        subprocess.check_call(["keybase", "pgp", "sign", "-d", "-i", "dist/{0}".format(i), "-o", "dist/{0}.asc".format(i)])

if should("pypi"):
    # upload to PyPI
    subprocess.check_call(["twine", "upload", "dist/*"])
