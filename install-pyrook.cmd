:: Windows install script for PyRook.
:: Assumes you have conda installed already.
cd \
@set && conda install pip setuptools pyqt lxml cffi pywin32 && pip install --upgrade --no-deps pyrook hunspell-cffi && pyrook || PAUSE
